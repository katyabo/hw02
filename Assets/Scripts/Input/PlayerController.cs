using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private Rigidbody playerRigidbody;
    private Vector3 startPosition;

    [SerializeField]
    private float moveSpeed = 600f;
    [SerializeField]
    private float rotationSpeed = 100f;
    [SerializeField]
    private float acceleration = 12000f;

    private void Awake()
    {
        playerInputActions = new PlayerInputActions();
        playerRigidbody = GetComponent<Rigidbody>();
        startPosition = transform.position;

        playerInputActions.Kart.ResetPosition.performed += context => ResetPosition();
        playerInputActions.Kart.Acceleration.started += context => AccelerationOn();
        playerInputActions.Kart.Acceleration.canceled += context => AccelerationOff();
    }

    private void OnEnable()
    {
        playerInputActions.Enable();
    }

    private void OnDisable()
    {
        playerInputActions.Disable();
    }

    void FixedUpdate()
    {
        Vector2 moveDirection = playerInputActions.Kart.Move.ReadValue<Vector2>();
        Move(moveDirection);

        Vector2 moveRotation = playerInputActions.Kart.Rotate.ReadValue<Vector2>();
        Rotate(moveRotation);

    }

    private void Move(Vector2 direction)
    {
        playerRigidbody.velocity = new Vector3(direction.x * moveSpeed * Time.fixedDeltaTime, 0f, direction.y * moveSpeed * Time.fixedDeltaTime);
    }

    private void ResetPosition()
    {
        playerRigidbody.MovePosition(startPosition);
        playerRigidbody.MoveRotation(Quaternion.identity);

    }

    private void Rotate(Vector2 rotation)
    {
        playerRigidbody.angularVelocity = new Vector3(0f, rotation.x * rotationSpeed * Time.fixedDeltaTime, 0f);
    }

    private void AccelerationOn()
    {
        moveSpeed += acceleration * Time.fixedDeltaTime * 20;
    }

    private void AccelerationOff()
    {
        moveSpeed -= acceleration * Time.fixedDeltaTime * 20;
    }
}
    
